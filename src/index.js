import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {browserHistory, Router, Route, IndexRoute} from 'react-router'

import './index.css';

import App from './App';
import Home from './scenes/Home';
import About from './scenes/About';

import registerServiceWorker from './registerServiceWorker';

injectTapEventPlugin();

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path='/' component={App}>
            <IndexRoute component={Home}/>
            <Route path='about' component={About}/>
        </Route>
    </Router>,
    document.getElementById('root'));
registerServiceWorker();

import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import logo from './logo.svg';
import SEAMGEN_LOGO from './seamgen-logo.svg';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
import * as Link from 'react-router/es/Link';
import * as Colors from 'material-ui/styles/colors';
import LinkContainer from 'react-router-bootstrap';
import './App.css';
import {FontIcon} from "material-ui";

function ReactLogo() {
    return <img src={logo} className="App-logo" alt="logo"/>;
}

function SeamgenLogo() {
    return <img src={SEAMGEN_LOGO} className="seamgen-logo" alt="seamgenLogo"/>;
}

function Courtesy() {
    return (
        <div>
            <label>Courtesy of </label>
            <SeamgenLogo/>
        </div>
    );
}

export default class App extends Component {

    srcPath = 'src/App.js';

    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false,
            mainTitle: 'Welcome to the React Material-UI Example Site'
        };

        this.handleDrawerRequest = this.handleDrawerRequest.bind(this);
    }

    openDrawer = () => {
        this.setState({
            isDrawerOpen: !this.state.isDrawerOpen
        });
    }

    handleDrawerRequest = (open, reason) => {
        if (reason === 'clickaway' || reason === 'escape') {
            this.handleDrawerClose();
        } else {
            console.log('isOpen ::: ' + open);
            console.log('reason ::: ' + reason);
        }


    }

    handleDrawerClose = () => {
        this.setState({
            isDrawerOpen: false
        });
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="App">
                    <Drawer docked={false}
                            open={this.state.isDrawerOpen}
                            handleClose={this.handleDrawerClose}
                            onRequestChange={this.handleDrawerRequest}>
                        <MenuItem>Menu Item</MenuItem>
                        <MenuItem
                            menuItems={<MenuItem>Beneath Home</MenuItem>}
                            insetChildren="true">SubMenu Example</MenuItem>
                    </Drawer>
                    <AppBar
                        title={this.state.mainTitle}
                        style={{backgroundColor: '#001b27', alignItems: 'center'}}
                        iconElementRight={<ReactLogo/>}
                        iconClassNameRight="muidocs-icon-navigation-expand-more"
                        showMenuIconButton="true"
                        onLeftIconButtonTouchTap={this.openDrawer}>
                    </AppBar>
                    <header className="App-header container">
                        <Courtesy/>
                    </header>
                    <div className="App-container">
                        {this.props.children}

                    </div>
                    <footer className="App-footer">
                        To get started, edit <code>{this.srcPath}</code> and save to reload.
                        <div className="flexcontainer">
                            <a style={{flexGrow: 1}} href="/">Home</a>
                            <a style={{flexGrow: 1}} href="/About">About</a>
                        </div>
                    </footer>
                </div>
            </MuiThemeProvider>
        );
    }
}
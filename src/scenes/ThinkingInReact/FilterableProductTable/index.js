/**
 * Created by jacob on 10/6/17.
 */
import React from 'react';
import {Component} from "react/cjs/react.production.min";
import ProductTable from "./ProductTable/index";
import SearchBarCheck from "./SearchBarCheck/index";

export default class FilterableProductTable extends Component {
    constructor(props) {
        super(props);
        this.state = {showWarning: true}
        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    handleToggleClick() {
        this.setState(prevState => ({
            showWarning: !prevState.showWarning
        }));
    }

    /**
     * Setup SearchBarCheck to do a filter on the API result set.
     * Setup product table to only display the results of the filter from this point forward.
     * @returns {XML}
     */
    render() {
        return (
            <div>
                <SearchBarCheck/>
                <ProductTable />
            </div>
        );
    }
}
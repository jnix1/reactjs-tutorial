/**
 * Created by jacob on 10/6/17.
 */
import React from 'react';
import {Component} from "react/cjs/react.production.min";
import SearchBar from 'material-ui-search-bar'


const styles = {
    block: {
        maxWidth: 250,
    },
    checkbox: {
        marginBottom: 16,
    },
};

export default class SearchBarCheck extends Component {

    constructor(props) {
        super(props);
        this.state = {showWarning: true}
        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    handleToggleClick() {
        this.setState(prevState => ({
            showWarning: !prevState.showWarning
        }));
    }

    render() {
        return (
            <div style={styles.block}>
                <SearchBar onChange={() => console.log('onChange')}
                           onRequestSearch={() => console.log('onChange')}/>
                <Checkbox
                    label="Simple"
                    style={styles.checkbox}
                />
            </div>
        );
    }
}
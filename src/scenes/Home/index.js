/**
 * Created by jacob on 10/5/17.
 */
import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';


export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {isDrawerOpen: false};

        this.handleDrawerRequest = this.handleDrawerRequest.bind(this);
    }


    columnClass = {
        flexGrow: 1
    }

    openDrawer = () => {
        this.setState({
            isDrawerOpen: !this.state.isDrawerOpen
        });
    }

    handleDrawerRequest = (open, reason) => {
        if (reason === 'clickaway' || reason === 'escape') {
            this.handleDrawerClose();
        } else {
            console.log('isOpen ::: ' + open);
            console.log('reason ::: ' + reason);
        }


    }

    handleDrawerClose = () => {
        this.setState({
            isDrawerOpen: false
        });
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="App">

                    <div className="App-intro container">
                        <div style={this.columnClass}>
                            <RaisedButton label="Material UI"/>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 2</label>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 3</label>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 4</label>
                        </div>
                    </div>
                    <div className="App-intro container">
                        <div style={this.columnClass}>
                            <label>Column 1</label>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 2</label>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 3</label>
                        </div>
                        <div style={this.columnClass}>
                            <label>Column 4</label>
                        </div>
                    </div>

                </div>
            </MuiThemeProvider>
        );
    }
}
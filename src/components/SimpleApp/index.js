/**
 * Created by jacob on 10/4/17.
 */
import React, {Component} from 'react';
import logo from '../logo.svg';
import '../App.css';
import Calculator from './components/Calculator/Calculator.component';

function Welcome(props) {
    return <h1>Welcome to React, {props.name}</h1>;
}

function Avatar(props) {
    return (
        <img className="Avatar"
             src={props.user.avatarUrl}
             alt={props.user.name}
        />
    );
}

function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user}/>
            <div className="UserInfo-name">
                {props.user.name}
            </div>
        </div>
    );
}

function formatDate(date) {
    return date.toLocaleDateString();
}

function Comment(props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author}/>
            <div className="Comment-text">
                {props.text}
            </div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}

function FormattedDate(props) {
    return <h2>It is {props.date.toLocaleTimeString()}.</h2>;
}

function ListItem(props) {
    return <li>{props.value}</li>;
}

function NumberList(props) {
    const numbers = props.numbers;

    return (
        <ul>
            { numbers.map((number) =>
                <ListItem key={number.toString()}
                          value={number}>
                </ListItem>
            )}
        </ul>
    );
}

class Clock extends Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }


    /**
     * Component life cycle hook, The componentDidMount() hook runs after the component
     * output has been rendered to the DOM. This is a good place to set up a timer:
     */
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    /**
     * OnComponentDestroy
     */
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {

        /**
         * Wrong
         * this.state.comment = 'Hello';
         *
         * Correct
         * this.setState({comment: 'Hello'});
         *
         * Only capable of update through constructor, no direct changes.
         */
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <div>
                <h1>Tick Example</h1>
                <FormattedDate date={this.state.date}/>
            </div>
        );
    }
}

export default class SimpleApp extends Component {
    srcPath = 'src/App.js';

    comment = {
        date: new Date(),
        text: 'I hope you enjoy learning React!',
        author: {
            name: 'Hello Kitty',
            avatarUrl: 'http://placekitten.com/g/64/64'
        }
    };

    numbers = [1, 2, 3, 4, 5];

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <Welcome className="App-title" name="Jacob"/>
                </header>
                <div className="App-intro">
                    <Calculator/>
                </div>
                <footer className="App-footer">
                    To get started, edit <code>{this.srcPath}</code> and save to reload.
                </footer>
            </div>
        );
    }
}
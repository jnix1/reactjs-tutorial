/**
 * Created by jacob on 10/3/17.
 */

import React from 'react';
import {Component} from "react/cjs/react.production.min";

export default class Reservation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isGoing: true,
            numberOfGuests: 2
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div>
                <form>

                    <label>
                        Is going:
                        <input
                            name="isGoing"
                            type="checkbox"
                            checked={this.state.isGoing}
                            onChange={this.handleInputChange}/>
                    </label>
                    <br />
                    <label>
                        Number of guests:
                        <input
                            name="numberOfGuests"
                            type="number"
                            value={this.state.numberOfGuests}
                            onChange={this.handleInputChange}/>
                    </label>
                </form>
                <label>IsGoing ::: {this.state.isGoing ? 'true' : 'false'}</label>
                <label>NoGuests ::: {this.state.numberOfGuests}</label>
            </div>
        );
    }
}
